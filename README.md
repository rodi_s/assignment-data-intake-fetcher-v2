# Parser assignment #

This repository contains a parser application that can fetch data from a Deliveroo restaurant page. Running the application will start a script which makes a web request to a Deliveroo page. If the request succeeds it will return the contents of the webpage. These contents are then parsed into useful data. This data is used elsewhere in the company. The application currently retrieves the following data: The name of a restaurant and all menu items consisting of the item's name, description and price. The end user will supply input URL's and then wait for it's output, the logic inside the scripts is black box logic for the end users.

## Pre-assignment requirements

Before starting the assignment make sure the following requirements are fulfilled.

- You have a working computer with a working internet connection
- GIT is installed
- Python 3.10 or higher is installed
- You have an IDE installed which can be used for Python development
- The libraries specified in the `requirements.txt` file are installed
- (Optional) Docker is installed and running on your system

## Setup

Before starting on the project make sure that the 'Pre-assignment requirements' are fulfilled.

If the requirements are fulfilled simply run the `main.py` script to run the application. Starting the project from another location will result in import errors.

Additionally it is possible to create a docker image of this project using the included Dockerfile, created with 3.10-slim. The image can be used to run the application inside a container.

The unit tests can be ran by changing ENV_MODE to 'test'.

## Introduction

The application does currently run and can fetch data from Deliveroo webpages. However, it has a few problems. The original developer of this application has left the company and there's nobody who knows how her code works.

Some of the problems with the application have been found by the people who execute the script, these have been listed under the 'issues' section below. There are more issues with the application than the ones listed below, but they simply haven't been noticed yet by the people in the company.

Additionally, the CEO has requested for some extra features to be added A.S.A.P. These must have been added in the next update. The feature requests are listed under the 'features' section below.

## Assignment 

The assignment is as follows. 

- Implement as many of the feature requests listed below as possible.
- Fix as many of the listed issues as possible. 
- If you encounter unlisted issues with the code please add comments in your code describing them so that you or another developer can fix these in the future.

We might want to fetch data from another source in the future as well. Your components should therefore be as reusable as possible.

You should spend 1-2 hours on this assignment, definitely not more than 2 hours in total. Don't worry if you run out of time and can't get everything done. We will discuss any problems you encountered, your ideas, your observations and your plans during the interview.

Clone this repository to your development environment and add your modifications to the code. Create a ZIP file of this project and send this ZIP file to us when you are done. Alternatively you could create a repository on your own your preferred version control platform, upload your solution there and send us a link of your repository.

### Feature requests

* A timestamp should be added to the data output so that we have a timestamp for the data item.
* Each fetched restaurant should get an ID assigned to it. This ID needs to be unique for a restaurant. If the restaurant is fetched again in the future, the ID needs to be identical to the one generated in the past.
* The application currently only prints it's output in the console. The output should be stored in a JSON file within the `/output` folder. Each restaurant's file should get it's own unique name.
* The average rating and amount of reviews should now also be retrieved from a restaurant's wegpage and added to the output data.


### Known issues (Bugs)

* Currently webpages that don't work anymore are still retrieved and shown in the output, we want to ignore webpages that don't work. For example: https://deliveroo.nl/nl/menu/rotterdam/centrum/cafe-datlinq should not be delivered in the output because it doesn't exist.
* Some fields in the printed `menu` only contain `{'@Type:':'MenuItem'}`, these should not be in the output.
* When our internet is down the parser crashes, we don't want it to crash when the internet goes down.