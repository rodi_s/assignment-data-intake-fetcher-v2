from loguru import logger
from typing import Union
from lxml.etree import _ElementTree

from services.request_service import get_response, parse_response


class DeliverooParser():
    def __init__(self):
        pass
                

    def _parse_deliveroo(self, tree: _ElementTree, url: str) -> dict:
        """Parses the content from a deliveroo restaurant.

        Args:
            url (str): The URL for the webpage.
            tree (ElementTree): An [ElementTree](https://python.readthedocs.io/en/v2.7.2/library/xml.etree.elementtree.html) object containing the parsed contents of the webpage.

        Returns:
            restaurant_data (dict): A dictionary holding the data.       
        """
        Restaurant = {'@Type':'Restaurant'}

        Restaurant['url'] = url
        name = tree.xpath("//h1")
        Restaurant['name'] = name[0].text

        menu = tree.xpath("//div[contains(@class, 'UIMenuItemCard')]/*/*")
        
        menu_list = []
        
        for item in menu:
            MenuItem = {'@Type':'MenuItem'}
            name = item.find("p")
            if name is not None: MenuItem['name'] = name.text
            description = item.find("span/p")
            if description is not None: MenuItem['description'] = description.text
            prices = item.find("p/span")
            if prices is not None: MenuItem['prices'] = prices.text
            menu_list.append(MenuItem)
            
        Restaurant['menu'] = menu_list 
       
        return Restaurant


    def parse(self, url: str) -> Union[bool, dict]:
        """Parses the website and returns the output.

        Args:
            url (str): The URL that has to be parsed.

        Returns:
            bool: True or False depending on whether the parsing succeeded.
            dict: Dictionary containing the parsed data.
        """
        response = get_response(url, header={ 
            'user-agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.99 Safari/537.36'
            ,'accept' : '*/*'
            ,'accept-encoding': 'gzip, deflate, br'
            ,'Connection': 'keep-alive'})
        
        tree = parse_response(response)

        output = self._parse_deliveroo(tree, url)
        return True, output
