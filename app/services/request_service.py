from requests import get, Response
from requests.exceptions import SSLError, Timeout
from loguru import logger
from lxml.etree import _ElementTree, HTMLParser, parse, ParseError
from io import StringIO


def get_response(url: str, **kwargs) -> Response | None:
    """Send a GET request using `requests.get` then returns the response object. Returns None upon failure.

    Args:
        `url (str)`: The URL for the GET request.
        `**header (dict, optional, default=default_header)`: The header to be used with the GET request. Default header is hardcoded, set to `{'user-agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.38', 'accept-language' : 'en-US;q=0.8,en;q=0.7'}`
        `**timeout (int, optional, default=20)`: Treshhold for long the GET request should wait for a response in seconds, throws an exception when treshhold is reached. Note that RabbitMQ may throw an exception if this timeout is set for too long and reached causing a RabbitMQ timeout as well. 
        `**allow_redirects (bool, optional, default=True)`: Specify whether the request is allowed to redirect the GET request to another page.

    Returns:
        `Response` [Response](https://docs.python-requests.org/en/latest/api/#requests.Response): Response object for the requests library.
        `None`: Returns None instead of a response object if an exception was raised.
        
    Raises:
        `SSLError`: Throws an SSL error.
        `Exception`: Error.
        `Timeout`: Thrown when a timeout for the request has been reached.
    """
    default_header = { 'user-agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/93.0.4577.63 Safari/537.36 Edg/93.0.961.38', 'accept-language' : 'en-US;q=0.8,en;q=0.7'}
    header = kwargs.get('header', default_header)
    timeout = kwargs.get('timeout', 20)
    redirect = kwargs.get('allow_redirects', True)
    
    try:
        logger.debug("[1/2] Sending request")
        response = get(url, timeout=timeout, headers=header, allow_redirects=redirect)
        logger.debug("[2/2] Sucesfully received response.")
        return response
    except (SSLError, Exception, Timeout) as e:
        logger.error(e)
        logger.error("[X] Exception raised in GET request.")
        return None


def parse_response(response: Response) -> _ElementTree | None:
    """Parses a `requests.Response` object into a an [ElementTree](https://python.readthedocs.io/en/v2.7.2/library/xml.etree.elementtree.html) object.

    Args:
        `response` [Response](https://docs.python-requests.org/en/latest/api/#requests.Response): A `response` object.

    Returns:
        `ElementTree` [ElementTree](https://python.readthedocs.io/en/v2.7.2/library/xml.etree.elementtree.html): The [ElementTree](https://python.readthedocs.io/en/v2.7.2/library/xml.etree.elementtree.html) from the response object.
        `None`: Returns None if the parsing fails.     
        
    Raises:
        `ParseError`: Throws a ParseError.
        `Exception`: Error.
    """
    try:
        logger.debug("Parsing response into ElementTree")
        encoding = response.encoding
        html = response.content.decode(encoding)

        parser = HTMLParser()
        tree = parse(StringIO(html), parser)
    
        return tree
    except (ParseError, Exception) as e:
        logger.error(e)
        logger.error("Failed parsing a response into ElementTree")
        return None

    
    



