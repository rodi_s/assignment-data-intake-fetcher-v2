import pytest
from loguru import logger

from fetcher.deliveroo import DeliverooParser

URL_GOOD = "https://deliveroo.nl/nl/menu/rotterdam/centrum/kfc-binnenweg"
URL_BAD = "https://deliveroo.nl/nl/menu/rotterdam/centrum/cafe-datlinq"

ENV_MODE = "dev" # dev prod or test

def main():
    logger.debug("========================= Starting parsing =========================")
    
    deliveroo_parser = DeliverooParser()
    
    success, output = deliveroo_parser.parse(URL_GOOD)
    if success: logger.info(output)
    else: logger.error(f"Bad input: {URL_GOOD}")
    
    success, output = deliveroo_parser.parse(URL_BAD)
    if success: logger.info(output)
    else: logger.error(f"Bad input: {URL_BAD}")
    
    logger.debug("========================= Finished parsing =========================")
    

if __name__ == "__main__":
    if ENV_MODE in ("dev", "prod"):
        main()
    if ENV_MODE in ("test"):
        pytest.main()